# A Flow3r MPD client

## How to use

Add a `/sd/mpd.toml` with:
```
host = "mpd"
port = 6600
```

## Album art

flow_mpd supports album art but crashes when the images are to big.
The easiest workaround is resizing the images on the MPD server using:
```
set globstar
mogrify -resize 240x240 **/cover.*
```
The bug has been reported as:
https://git.flow3r.garden/flow3r/flow3r-firmware/-/issues/171

## Key bindings

Flow3r buttons are numbered clockwise starting from the top leaf with the USB port.

### Global keybindings

0. play/pause
1. increase volume
3. decrease volume

### In the main view

2. next song
4. view playlist queue
5. list albums
6. list artists
7. list stored playlists
8. previous song

### In \*list view

2. add current selection to playlist queue
5. clear queue
6. switch to main view
8. scroll list
9. enter selected directory


## How to auto start

In `/flash/sys/st3m/run.py` change:

```
override_main_app: Optional[str] = "Flow MPD"
```
